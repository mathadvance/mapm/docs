# Installation

If you have Rust, just run `cargo install mapm-cli`. This is the recommended way to install mapm, particularly because the pre-install script in the package will automatically generate a preview template, script, and `mapm.sty` for you.

Otherwise, binaries for each major version can be found on the [mapm GitLab](https://gitlab.com/mapm/cli/-/releases). Follow the instructions for installing `mapm.sty` in the Dependencies section too.

## Dependencies

You must have a working installation of TeX, preferably TeX Live with the `full` installation scheme. You must also have the `latexmk` script installed. Typically this comes with TeX Live.

You will also need to configure the problem directory; for details on that, see the corresponding section in the [problems](/problems) page.
