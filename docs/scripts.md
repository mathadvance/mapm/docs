# Post-build scripts

Post-build scripts, as the name suggests, run after building a contest.

For Unix-like systems, if you run `mapm build` with template `TEMPLATE`, it will
execute ~/.config/mapm/scripts/TEMPLATE (Linux) or ~/Library/Application\
Support/mapm/scripts/TEMPLATE (Mac). Said script file should be a bash script.

On Windows, scripts are invoked using PowerShell. This should not be too big a
deal, since post-build scripts should not be too complicated (in particular,
it's probably just a wrapper for a PDF viewer).

For mapm preview, the template is "preview", and for mapm preview-all, the
template is "preview-all".

These post-build scripts are *not* included in the template directory for a
couple of reasons:
- They are OS-specific, unlike template directories.
- They are not meant to be very sophisticated. They only exist as a wrapper for
  Mac OS/Linux users whose PDF viewer isn't a default browser, so they can
  query for a process with the name `preview.pdf` instead of unnecessarily
  running another preview process.
