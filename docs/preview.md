# Preview

This information is pertinent to setting up the `mapm preview` command.

## Contest

Under the hood, the contest with the following yaml is compiled:
```yaml
template: preview
vars: {}
problems:
  - PROBLEM
```
where PROBLEM is the name of the problem passed into the `preview` command.

## Template

The `vars` key of your template must be empty (because the contest has no variables), but besides that you have totally free reign. For instance, you can (and should) enforce some problem and solution variables.

## Script

When running `mapm preview`, the script with name `preview` is run. By default, it opens your preferred PDF viewer. The exact default command is OS-specific.
