# Commands

The binary that you install is called `mapm` by default.

When running `view` or `find`, the program will look for the `PAGER` environment variable and pipe the output through it, and if none is found, it will just print the contents in your terminal. It also respects `NO_COLOR` [(a description of NO_COLOR)](https://no-color.org/).

If you are using `less` as your pager, you will also want to set `LESS='-R'` to interpret color codes (unless you also have `NO_COLOR` set).

## --help

**Usage:** `mapm -h` for short help and `mapm --help` for long help

Displays the help page.

If the `--help` flag is passed for a subcommand, it will display the help page for that specific subcommand.

## profile

**Usage:** `mapm profile <set PROFILENAME/get>`

Gets or sets the active profile. Under the hood, `mapm` is just working with the file `$CONFIG/profile`.

### list

**Usage:** `mapm profile list`

Lists all profiles in the mapm directory.

### get

**Usage:** `mapm profile get`

Prints the active profile.

### set

**Usage:** `mapm profile set [PROFILENAME]`

Sets the active profile to `PROFILENAME`, if one is passed in. If not, then one is 

## build

**Usage:** `mapm build [FILES...]`

Builds the contest associated with FILE, which should be a `.yml`. Multiple contest directories can be passed in, but this is not recommended.

`mapm` will throw an error if the problem count is wrong or some problem or its solution has undefined variables.

## view

**Usage:** `mapm view [PROBLEM_NAMES...] --hide/show [KEYS...]`

An example is
```sh
$ mapm view alexander-balls
```

By default, `view` will show every attribute of the problem. To modify this, you can either use the `hide` or `show` subcommand.

The hide and show subcommands are **mutually exclusive**.

You may pass in multiple problems.

### show

If you want to show certain attributes, you should use the `show` subcommand. For instance, to show the `difficulty` attribute of a problem, you could run
```sh
$ mapm view alexander-balls --show difficulty
```

### hide

Perhaps you want to view every attribute of the problem besides the solutions. You could run
```sh
$ mapm view alexander-balls --hide solutions
```

## find

**Usage:** `mapm find [KEY<OPERATOR>VALUE...] --hide/show [KEYS...]`

Returns all problems that satisfy **every** `KEY<OPERATOR>VALUE` argument. Valid values of `<OPERATOR>` are `=`, `<`, `>`, `<=`, `>=`.

If no `KEY<OPERATOR>VALUE` arguments are passed, then every problem is returned.

An example is
```sh
$ mapm find difficulty=3
```
which will return the name of every problem with difficulty 3.

To check for existence of a key, you can just use `mapm find --KEY`. So to look for every problem with the `solutions` key (which implies they have a solution), run
```sh
$ mapm find solutions
```

You can check for non-equality or non-existence of a key by prepending it with an `!` character. For instance,
```sh
$ mapm find !difficulty=3
```
returns all problems without difficulty 3. If you specifically want a difficulty to exist but want it *not* to be 3, then you can try
```sh
$ mapm find difficulty !difficulty=3
```

If your valid difficulties are 1-5, then you can get all problems with difficulties 1 and 2 by running
```sh
$ mapm find difficulty !difficulty=3 !difficulty=4 !difficulty=5
```

Since all your difficulties are integers (which is necessary in order to use any comparison operator besides `=`), you may also run
```sh
$ mapm find difficulty<=2
```
or
```sh
$ mapm find difficulty<3
```

If you want to check for problems that don't have solutions (so you can write them), run
```sh
$ mapm find !solutions
```

Each flag with a comparison operator implicitly requires the key to exist in the problem file. Thus, to get all problems with a difficulty not set to 3, run
```sh
$ mapm find difficulty !difficulty=3
```

### show

The functionality is identical to `show` from `view`.

### hide

The functionality is identical to `hide` from `view`.

## edit

**Usage**: `mapm edit [PROBLEM_NAMES]...`

For example, to edit the problem `alexander-balls`, run
```sh
mapm edit alexander-balls
```

On Windows the command opens Notepad, and for Unix the `EDITOR` environment variable is used.

You can edit multiple problems at once by passing in multiple problem names. For instance,
```sh
mapm edit alexander-balls alien-ages
```

## rename

**Usage**: `mapm rename <TARGET> <DESTINATION>`

Does exactly what it sounds like. You do not need to, and should not, pass in the `.yml` extension at the end.

## delete

**Usage**: `mapm delete <PROBLEM_NAMES>...`

Same instructions as `rename`.

## preview

**Usage**: `mapm preview <PROBLEMS>...`

Compiles a preview PDF of the problem passed in. This is based on the [preview template](/preview).

## preview-all

**Usage**: `mapm preview-all [KEY<OPERATOR>VALUE]... --hide/show [KEYS]...`

The usage is identical to `mapm find`. Note that the code that generates the `preview-all` PDF, *which you are strongly encouraged to modify for your own use*, requires all problems to have the `problem` and `author` keys, and requires all solutions to have the `text` and `author` keys. **The `hide` and `show` commands also must show the `problem` and `author` keys**, and mapm does *not* automatically show them for you (so if for whatever reason you want to hide the author, you can).
